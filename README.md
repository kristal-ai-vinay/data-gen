# data-gen

Data Gen app - Used to generate random prices for every stocks at every second and the same is being pushed to kafka topic named #stock-details

# Prerequisites
1. Download / clone https://github.com/confluentinc/cp-all-in-one
2. cd cp-all-in-one
3. git checkout 5.4.1-post
4. cd cp-all-in-one
5. docker-compose up -d

This will boot-up Kafka server, Schema registry server, Zookeeper

Ref: https://docs.confluent.io/current/quickstart/ce-docker-quickstart.html

# Run Application
1. sh runner.sh
