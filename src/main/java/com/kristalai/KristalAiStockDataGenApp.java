package com.kristalai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.cloud.stream.schema.client.EnableSchemaRegistryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(exclude = {
    org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration.class
})
@EnableBinding(Source.class)
@EnableSchemaRegistryClient
@EnableScheduling
public class KristalAiStockDataGenApp {

  public static void main(String[] args) {
    SpringApplication.run(KristalAiStockDataGenApp.class, args);
  }

}
