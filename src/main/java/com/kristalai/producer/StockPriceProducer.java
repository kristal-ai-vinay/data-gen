package com.kristalai.producer;

import java.time.Instant;

import com.kristalai.schema.StockDetails;
import io.github.resilience4j.retry.Retry;
import lombok.AllArgsConstructor;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class StockPriceProducer {

  private final Source source;

  public void produceStockPriceDetails(String scrip, Instant time, float ltp) {
    StockDetails stockDetails = new StockDetails();
    stockDetails.setScrip(scrip);
    stockDetails.setLtp(ltp);
    stockDetails.setTimeStamp(time);
    Message<StockDetails> message = MessageBuilder.withPayload(stockDetails).build();
    // Added resilience if kafka broker is not available / connectivity issues will do retry
    Retry.ofDefaults("publishMessageOperation")
        .executeRunnable(() -> source.output().send(message));
  }
}
