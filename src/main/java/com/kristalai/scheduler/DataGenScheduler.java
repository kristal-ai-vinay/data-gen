package com.kristalai.scheduler;

import java.text.DecimalFormat;
import java.time.Instant;
import java.util.Map;
import java.util.Random;

import com.kristalai.producer.StockPriceProducer;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class DataGenScheduler {

  private final StockPriceProducer stockPriceProducer;

  private final Random rand = new Random();

  private static final Map<String, Float> ALL_SCRIPS = Map
      .of("SBIN", 205.0f, "INFY", 980.0f, "TCS", 2600.0f, "IOC", 87.0f, "HINDALCO", 188.0f);

  private final int MIN_PRICE = -3;

  private final int MAX_PRICE = 3;

  @Scheduled(cron = "* * * * * *")
  public void cronJobScheduler() {
    Instant now = Instant.now();
    log.info("Started generating data for all scrips at {}", now);
    ALL_SCRIPS.forEach((k, v) -> {
      float ltp = MIN_PRICE + rand.nextFloat() * (MAX_PRICE - MIN_PRICE);
      DecimalFormat df = new DecimalFormat("#.00");
      ltp = Float.parseFloat(df.format(ltp));
      ltp = v + ltp;
      stockPriceProducer.produceStockPriceDetails(k, now, ltp <= 0 ? 0.05f : ltp);
    });
  }
}
